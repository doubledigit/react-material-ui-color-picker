"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ColorPicker = _interopRequireDefault(require("./components/ColorPicker"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _ColorPicker["default"];
exports["default"] = _default;